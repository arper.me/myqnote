#include "Manager/Manager.h"
#include <QApplication>
#include <QTranslator>
#include <QtCore/QDir>
#include <QtCore/QLibraryInfo>

int main(int argc, char *argv[]) {

    QDir myPath(".mykeepdata");

    QTranslator translator;

    QTranslator systemTranslator;
    systemTranslator.load("qt_" + QLocale::system().name(),
                          QLibraryInfo::location(QLibraryInfo::TranslationsPath));

    QApplication app(argc, argv);
    app.installTranslator(&translator);
    app.installTranslator(&systemTranslator);

    Manager manager(myPath);
    manager.connectTraslator(&translator);
    manager.show();

    return app.exec();
}

