#include "Manager.h"
#include "./ui_manager.h"
#include <QDebug>
#include <QtWidgets/QInputDialog>
#include <QMessageBox>
#include <stdexcept>
#include <QCloseEvent>
#include <QtGui/QDesktopServices>


Manager::Manager(QDir ubi, QWidget *parent)
        : QMainWindow(parent), ui(new Ui::Manager), _filesDir(ubi), _translator(nullptr), _editor(this),
          _model(&_list), _selectIndex(-1), _currentView(myView::visibles), _notesDir(_filesDir),
          _iconsDir(QString(_filesDir.path()).append("/resources/icons")) {

    ui->setupUi(this);

    // Compruebo si existe la carpeta para las notas, sino la creo
    if (!_notesDir.cd("notes")) {
        _notesDir.mkdir("notes");
        _notesDir.cd("notes");
    }


    // Load Images
    this->loadReloadImages();


    // Connections
    connect(ui->actionExit, &QAction::triggered, this, &QWidget::close);
    connect(ui->actionLanguage, &QAction::triggered, this, &Manager::selectorIdiomas);
    connect(ui->listView, &QListView::doubleClicked, this, &Manager::doubleClickElement);
    connect(ui->listView, &QListView::clicked, this, &Manager::selectElement);
    connect(ui->rButtonVisibles, &QRadioButton::clicked, this, &Manager::actionVisible);
    connect(ui->rButtonHidden, &QRadioButton::clicked, this, &Manager::actionHidden);
    connect(ui->rButtonAll, &QRadioButton::clicked, this, &Manager::actionAll);
    // Ocultar archivo
    connect(ui->hideButton, &QRadioButton::clicked, this, &Manager::actionHide);
    connect(ui->actionVisibility, &QAction::triggered, this, &Manager::actionHide);
    // Añadir nota
    connect(ui->addButton, &QRadioButton::clicked, this, &Manager::actionAdd);
    connect(ui->actionAdd, &QAction::triggered, this, &Manager::actionAdd);
    // Elminar nota
    connect(ui->delButton, &QRadioButton::clicked, this, &Manager::actionDel);
    connect(ui->actionDel, &QAction::triggered, this, &Manager::actionDel);
    // Cambiar nombre
    connect(ui->renameButton, &QRadioButton::clicked, this, &Manager::actionChangeName);
    connect(ui->actionRename, &QAction::triggered, this, &Manager::actionChangeName);

    // Help
    connect(ui->actionHelp, &QAction::triggered, this, &Manager::help);
    connect(ui->actionAbout, &QAction::triggered, this, &Manager::about);

    // Tabla
    ui->listView->setModel(&_model);
    ui->rButtonVisibles->click();

}

Manager::~Manager() {
    delete ui;
}

void Manager::loadReloadImages() {

    // todo Atribuir autor <div>Iconos diseñados por <a href="https://www.flaticon.es/autores/popcorns-arts" title="Icon Pond">Icon Pond</a> from <a href="https://www.flaticon.es/" title="Flaticon">www.flaticon.es</a></div>

    // Icon Window
    this->setWindowIcon(QIcon(getIconsDir().path() + "/" + tr("keeps.png")));

    // Icons
    ui->actionHelp->setIcon(QIcon(getIconsDir().path() + "/" + tr("help.png")));
    ui->actionAbout->setIcon(QIcon(getIconsDir().path() + "/" + tr("about.png")));
    ui->actionAdd->setIcon(QIcon(getIconsDir().path() + "/" + tr("add.png")));
    ui->actionDel->setIcon(QIcon(getIconsDir().path() + "/" + tr("del.png")));
    ui->actionExit->setIcon(QIcon(getIconsDir().path() + "/" + tr("quit.png")));
    ui->actionRename->setIcon(QIcon(getIconsDir().path() + "/" + tr("rename.png")));
    ui->actionVisibility->setIcon(QIcon(getIconsDir().path() + "/" + tr("visibility.png")));
    ui->actionLanguage->setIcon(QIcon(getIconsDir().path() + "/" + tr("language.png")));

    ui->addButton->setIcon(QIcon(getIconsDir().path() + "/" + tr("add.png")));
    ui->delButton->setIcon(QIcon(getIconsDir().path() + "/" + tr("del.png")));
    ui->renameButton->setIcon(QIcon(getIconsDir().path() + "/" + tr("rename.png")));
    ui->hideButton->setIcon(QIcon(getIconsDir().path() + "/" + tr("visibility.png")));



}

QStringList Manager::listDirectory(QDir dir, QString extension, bool listHidden) {

    QDir directorio(dir);
    directorio.setFilter(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);

    if (listHidden) directorio.setFilter(directorio.filter() | QDir::Hidden);

    QStringList filters;
    filters << QString().append("*.").append(extension);
    directorio.setNameFilters(filters);


    // Comprobar si existe el directorio.
    if (!directorio.exists()) {
        QMessageBox::critical(this, tr("Directorio"), tr("El directorio no existe"));
        return QStringList();
    }

    return directorio.entryList();
}

void Manager::selectorIdiomas() {

    QDir dir(getFilesDir());
    dir.cd("resources/languages");

    QStringList idiomas = listDirectory(dir);

    // Comprobar si hay idiomas en el directorio.
    if (idiomas.isEmpty()) {
        QMessageBox::warning(this, tr("Idiomas"), tr("No se han encontrado idiomas.\nIdioma por defecto."));
        return;
    }

    if (!(_translator)) throw std::invalid_argument("Translator ins't correct.");


    idiomas.replaceInStrings(".qm", "");

    // Selección del idioma.
    QString seleccionado = QInputDialog::getItem(this, tr("Lenguaje"), tr("Selecciona el idioma: "), idiomas, 0,
                                                 false).append(".qm");

    if (!_translator->load(dir.path() + "/" + seleccionado)) {
        QMessageBox::warning(this, tr("Idioma"), tr("No se ha podido cambiar el idioma.\nIdioma por defecto."));
        return;
    }

    // Traducir interfaces gráficas

    this->retranslateUi();
    _editor.retranslateUi();
}

void Manager::connectTraslator(QTranslator *tr) {
    _translator = tr;
}

void Manager::closeEvent(QCloseEvent *event) {

    if (_editor.isRunning()) _editor.close();

    else event->accept();


}

const QDir &Manager::getFilesDir() const {
    return _filesDir;
}

void Manager::retranslateUi() {
    this->loadReloadImages();
    ui->retranslateUi(this);
}

void Manager::edit(QString e) {
    if (_editor.open(this, e)) {
        _editor.show();
        this->setDisabled(true);
    }
}

void Manager::doubleClickElement(const QModelIndex &index) {
    edit(_list.at(index.row()));
}

void Manager::selectElement(const QModelIndex &index) {
    if (_list.at(index.row()).at(0) == '.') ui->hideButton->setText(tr("Mostrar"));
    else ui->hideButton->setText(tr("Ocultar"));

    _selectIndex = index.row();
}

void Manager::actionVisible() {

    _list = listDirectory(getNotesDir(), "note", false);

    clearSelectionFromListView();
    ui->hideButton->setText(tr("Ocultar"));

    _model.updateView();

    _currentView = myView::visibles;
}

void Manager::actionHidden() {

    QStringList l = listDirectory(getNotesDir(), "note");

    clearSelectionFromListView();
    ui->hideButton->setText(tr("Mostrar"));
    _list.clear();

    for (auto &i : l) {
        if (i.at(0) == '.') _list << i;
    }

    _model.updateView();

    _currentView = myView::ocultos;
}

void Manager::actionAll() {

    _list = listDirectory(getNotesDir(), "note");

    clearSelectionFromListView();

    _model.updateView();

    _currentView = myView::todos;
}

bool Manager::validName(QString &e) {

    if (e.isEmpty()) return false;

    for (int k = 0; k < e.count(); ++k) {
        if (e.at(k) == " ") {
            e.remove(k, 1);
            k = 0;
            continue;
        }
        if (e.at(k) == ".") return false;
        else break;
    }

    for (int j = e.count() - 1; 0 <= j; --j) {

        if (e.at(j) == " ") {
            e.remove(j, 1);
            j = e.count() - 1;
        } else break;

    }

    for (auto &i : listDirectory(getNotesDir(), "note")) {
        if (e == QString(i).remove(".note")) return false;
        if ((i.at(0) == '.') && (QString(i).remove(".note").remove(0, 1) == e)) return false;
    }
    return true;
}

void Manager::actionAdd() {

    QString name;

    do {
        bool ok;

        name = QInputDialog::getText(this, tr("Nombre de la nota"),
                                     tr("Nombre:"), QLineEdit::Normal, QString(), &ok);

        if (!ok) return;

        if (!validName(name))
            QMessageBox::warning(this, tr("Nombre no válido."),
                                 tr("El nombre no puede estar vacio ni empezar con '.'"));
    } while (!validName(name));

    QFile file;

    switch (_currentView) {
        case myView::ocultos:
            file.setFileName(getNotesDir().path() + QString("/.%2.note").arg(name));
            file.open(QIODevice::WriteOnly | QIODevice::Text);
            break;
        default:
            file.setFileName(getNotesDir().path() + QString("/%2.note").arg(name));
            file.open(QIODevice::WriteOnly | QIODevice::Text);
            break;
    }

    updateIt();

}

void Manager::actionDel() {

    if (_selectIndex != -1) {
        QFile archivo(getNotesDir().path() + QString("/").append(_list.at(_selectIndex)));

        if (!archivo.exists()) {
            QMessageBox::critical(this, tr("Info"), tr("El archivo no existe"));
            return;
        }

        QMessageBox::StandardButton resBtn = QMessageBox::question(this, tr("Eliminar"),
                                                                   tr("¿Eliminar nota?\n"),
                                                                   QMessageBox::Yes | QMessageBox::No,
                                                                   QMessageBox::Yes);


        if (resBtn == QMessageBox::Yes) {
            if (!archivo.remove()) QMessageBox::warning(this, tr("Info"), tr("La nota no se ha podido eliminar."));
            updateIt();

        }


    }
}

void Manager::actionChangeName() {

    if (_selectIndex != -1) {
        QFile archivo(getNotesDir().path() + QString("/").append(_list.at(_selectIndex)));

        if (!archivo.exists()) {
            QMessageBox::critical(this, tr("Info"), tr("El archivo no existe"));
            return;
        }

        QString name;

        do {
            bool ok;

            name = QInputDialog::getText(this, tr("Renombrar"),
                                         tr("Nuevo nombre:"), QLineEdit::Normal, QString(), &ok);

            if (!ok) return;

            if (!validName(name))
                QMessageBox::warning(this, tr("Nombre no válido."),
                                     tr("El nombre no puede estar vacio ni empezar con '.'"));
        } while (!validName(name));

        if (_list.at(_selectIndex).at(0) == '.') {
            archivo.rename(getNotesDir().path() + QString("/.%2.note").arg(name));
        } else {
            archivo.rename(getNotesDir().path() + QString("/%2.note").arg(name));
        }
        updateIt();
    }
}

void Manager::actionHide() {

    if (_selectIndex != -1) {
        QFile archivo(getNotesDir().path() + QString("/").append(_list.at(_selectIndex)));

        if (!archivo.exists()) {
            QMessageBox::critical(this, tr("Info"), tr("El archivo no existe"));
            return;
        }

        if (_list.at(_selectIndex).at(0) == '.') {
            archivo.rename(getNotesDir().path() + QString("/").append(QString(_list.at(_selectIndex)).remove(0, 1)));
        } else {
            archivo.rename(getNotesDir().path() + QString("/").append(QString(".").append(_list.at(_selectIndex))));
        }
        updateIt();
    }

}

void Manager::clearSelectionFromListView() {
    ui->listView->clearSelection();
    _selectIndex = -1;
}

void Manager::updateIt() {

    switch (_currentView) {
        case myView::visibles:
            this->actionVisible();
            break;
        case myView::ocultos:
            this->actionHidden();
            break;
        case myView::todos:
            this->actionAll();
            break;
    }
}

const QDir &Manager::getNotesDir() const {
    return _notesDir;
}

const QDir &Manager::getIconsDir() const {
    return _iconsDir;
}

void Manager::about() {

    QString about;

    QFile archivo(getFilesDir().path() + QString("/resources/about/").append(tr("es.txt")));

    archivo.open(QIODevice::ReadOnly | QIODevice::Text);

    if (!archivo.isOpen()) {
        QMessageBox::critical(this, tr("Error"), archivo.errorString());
        return;
    }

    QTextStream io;
    io.setDevice(&archivo);

    about = io.readAll();

    archivo.flush();
    archivo.close();

    QMessageBox msgBox(this);
    msgBox.setWindowIcon(QIcon(getIconsDir().path() + "/" + tr("about.png")));
    msgBox.setWindowTitle(tr("Acerca de"));
    msgBox.setTextFormat(Qt::RichText);
    msgBox.setText(about);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.exec();

}

void Manager::help() {
    QDesktopServices::openUrl(QUrl(tr("https://www.arper.me/myqnote/help/es.html")));
}
