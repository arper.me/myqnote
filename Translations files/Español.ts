<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES" sourcelanguage="es_ES">
<context>
    <name>Editor</name>
    <message>
        <location filename="Editor/editor.ui" line="14"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="137"/>
        <source>Editor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="42"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="172"/>
        <source>Arc&amp;hivo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="52"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="173"/>
        <source>Edi&amp;tar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="64"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="174"/>
        <source>Ay&amp;uda</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="76"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="175"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="100"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="138"/>
        <source>&amp;Guardar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="103"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="140"/>
        <source>Ctrl+G</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="108"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="142"/>
        <source>&amp;Salir</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="111"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="144"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="116"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="146"/>
        <source>&amp;Cortar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="119"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="148"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="124"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="150"/>
        <source>C&amp;opiar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="127"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="152"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="132"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="154"/>
        <source>&amp;Pegar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="135"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="156"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="140"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="158"/>
        <source>&amp;Deshacer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="143"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="160"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="148"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="162"/>
        <source>&amp;Rehacer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="151"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="164"/>
        <source>Ctrl+Shift+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="156"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="166"/>
        <source>&amp;Imprimir</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="159"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="168"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="164"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="170"/>
        <source>Ayuda</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="169"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="171"/>
        <location filename="Editor/Editor.cpp" line="193"/>
        <source>Acerca de</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="49"/>
        <source>editor.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="52"/>
        <source>save.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="53"/>
        <source>quit.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="54"/>
        <source>cut.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="55"/>
        <source>copy.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="56"/>
        <source>paste.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="57"/>
        <source>undo.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="58"/>
        <source>redo.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="59"/>
        <source>printer.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="60"/>
        <source>help.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="61"/>
        <location filename="Editor/Editor.cpp" line="192"/>
        <source>about.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="78"/>
        <location filename="Editor/Editor.cpp" line="99"/>
        <location filename="Editor/Editor.cpp" line="179"/>
        <source>Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="117"/>
        <source>Salir</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="118"/>
        <source>¿Guardar nota?
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="174"/>
        <source>es.txt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="202"/>
        <source>https://www.arper.me/myqnote/help/es.html</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ListModel</name>
    <message>
        <location filename="Manager/ListModel/ListModel.cpp" line="22"/>
        <source>No hay lista cargada.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Manager</name>
    <message>
        <location filename="Manager/manager.ui" line="14"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="187"/>
        <source>myQNote</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="27"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="205"/>
        <source>Mostrar notas:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="52"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="206"/>
        <source>Visibles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="68"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="207"/>
        <source>Ocu&amp;ltas</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="75"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="208"/>
        <source>Todas</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="101"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="209"/>
        <source>Añadir</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="110"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="210"/>
        <source>Cambiar nombre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="117"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="211"/>
        <location filename="Manager/Manager.cpp" line="298"/>
        <source>Eliminar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="124"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="212"/>
        <location filename="Manager/Manager.cpp" line="179"/>
        <location filename="Manager/Manager.cpp" line="189"/>
        <source>Ocultar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="143"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="213"/>
        <source>A&amp;rchivo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="152"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="214"/>
        <source>E&amp;ditar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="159"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="215"/>
        <source>&amp;Preferencias</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="165"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="216"/>
        <source>A&amp;yuda</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="177"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="188"/>
        <source>&amp;Añadir nota</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="182"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="189"/>
        <source>Cambiar &amp;nombre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="185"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="191"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="190"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="193"/>
        <source>&amp;Eliminar nota</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="193"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="195"/>
        <source>Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="198"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="197"/>
        <source>&amp;Salir</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="201"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="199"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="206"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="201"/>
        <source>&amp;Cambiar Idioma</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="211"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="202"/>
        <source>&amp;Mostrar/Ocultar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="216"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="203"/>
        <source>Ayuda</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="221"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="204"/>
        <location filename="Manager/Manager.cpp" line="419"/>
        <source>Acerca de</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="69"/>
        <source>keeps.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="72"/>
        <source>help.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="73"/>
        <location filename="Manager/Manager.cpp" line="418"/>
        <source>about.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="74"/>
        <location filename="Manager/Manager.cpp" line="81"/>
        <source>add.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="75"/>
        <location filename="Manager/Manager.cpp" line="82"/>
        <source>del.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="76"/>
        <source>quit.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="77"/>
        <location filename="Manager/Manager.cpp" line="83"/>
        <source>rename.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="78"/>
        <location filename="Manager/Manager.cpp" line="84"/>
        <source>visibility.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="79"/>
        <source>language.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="104"/>
        <source>Directorio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="104"/>
        <source>El directorio no existe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="120"/>
        <source>Idiomas</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="120"/>
        <source>No se han encontrado idiomas.
Idioma por defecto.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="130"/>
        <source>Lenguaje</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="130"/>
        <source>Selecciona el idioma: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="134"/>
        <source>Idioma</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="134"/>
        <source>No se ha podido cambiar el idioma.
Idioma por defecto.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="178"/>
        <location filename="Manager/Manager.cpp" line="201"/>
        <source>Mostrar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="261"/>
        <source>Nombre de la nota</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="262"/>
        <source>Nombre:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="267"/>
        <location filename="Manager/Manager.cpp" line="335"/>
        <source>Nombre no válido.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="268"/>
        <location filename="Manager/Manager.cpp" line="336"/>
        <source>El nombre no puede estar vacio ni empezar con &apos;.&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="294"/>
        <location filename="Manager/Manager.cpp" line="305"/>
        <location filename="Manager/Manager.cpp" line="320"/>
        <location filename="Manager/Manager.cpp" line="354"/>
        <source>Info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="294"/>
        <location filename="Manager/Manager.cpp" line="320"/>
        <location filename="Manager/Manager.cpp" line="354"/>
        <source>El archivo no existe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="299"/>
        <source>¿Eliminar nota?
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="305"/>
        <source>La nota no se ha podido eliminar.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="329"/>
        <source>Renombrar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="330"/>
        <source>Nuevo nombre:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="400"/>
        <source>es.txt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="405"/>
        <source>Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="428"/>
        <source>https://www.arper.me/myqnote/help/es.html</source>
        <translation></translation>
    </message>
</context>
</TS>
