<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="es_ES">
<context>
    <name>Editor</name>
    <message>
        <location filename="Editor/editor.ui" line="14"/>
        <location filename="Editor/editor.ui" line="14"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="137"/>
        <source>Editor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="42"/>
        <location filename="Editor/editor.ui" line="42"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="172"/>
        <source>Arc&amp;hivo</source>
        <translatorcomment>File</translatorcomment>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="52"/>
        <location filename="Editor/editor.ui" line="52"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="173"/>
        <source>Edi&amp;tar</source>
        <translatorcomment>Edit</translatorcomment>
        <translation>&amp;Edit</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="64"/>
        <location filename="Editor/editor.ui" line="64"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="174"/>
        <source>Ay&amp;uda</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="76"/>
        <location filename="Editor/editor.ui" line="76"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="175"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="100"/>
        <location filename="Editor/editor.ui" line="100"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="138"/>
        <source>&amp;Guardar</source>
        <translation>&amp;Save</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="103"/>
        <location filename="Editor/editor.ui" line="103"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="140"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="108"/>
        <location filename="Editor/editor.ui" line="108"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="142"/>
        <source>&amp;Salir</source>
        <translatorcomment>Exit</translatorcomment>
        <translation>E&amp;xit</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="111"/>
        <location filename="Editor/editor.ui" line="111"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="144"/>
        <source>Ctrl+Q</source>
        <translatorcomment>Ctrl+W</translatorcomment>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="116"/>
        <location filename="Editor/editor.ui" line="116"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="146"/>
        <source>&amp;Cortar</source>
        <translatorcomment>Cut</translatorcomment>
        <translation>Cut</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="119"/>
        <location filename="Editor/editor.ui" line="119"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="148"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="124"/>
        <location filename="Editor/editor.ui" line="124"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="150"/>
        <source>C&amp;opiar</source>
        <translatorcomment>Copy</translatorcomment>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="127"/>
        <location filename="Editor/editor.ui" line="127"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="152"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="132"/>
        <location filename="Editor/editor.ui" line="132"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="154"/>
        <source>&amp;Pegar</source>
        <translatorcomment>Paste</translatorcomment>
        <translation>Paste</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="135"/>
        <location filename="Editor/editor.ui" line="135"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="156"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="140"/>
        <location filename="Editor/editor.ui" line="140"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="158"/>
        <source>&amp;Deshacer</source>
        <translatorcomment>Undo</translatorcomment>
        <translation>Undo</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="143"/>
        <location filename="Editor/editor.ui" line="143"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="160"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="148"/>
        <location filename="Editor/editor.ui" line="148"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="162"/>
        <source>&amp;Rehacer</source>
        <translatorcomment>Redo</translatorcomment>
        <translation>Redo</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="151"/>
        <location filename="Editor/editor.ui" line="151"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="164"/>
        <source>Ctrl+Shift+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="156"/>
        <location filename="Editor/editor.ui" line="156"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="166"/>
        <source>&amp;Imprimir</source>
        <translatorcomment>Print</translatorcomment>
        <translation>&amp;Print</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="159"/>
        <location filename="Editor/editor.ui" line="159"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="168"/>
        <source>Ctrl+I</source>
        <translatorcomment>Ctrl+P</translatorcomment>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="164"/>
        <location filename="Editor/editor.ui" line="164"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="170"/>
        <source>Ayuda</source>
        <translatorcomment>Help</translatorcomment>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="Editor/editor.ui" line="169"/>
        <location filename="Editor/editor.ui" line="169"/>
        <location filename="compilacion/myQNote_autogen/include/ui_editor.h" line="171"/>
        <location filename="Editor/Editor.cpp" line="193"/>
        <source>Acerca de</source>
        <translatorcomment>About</translatorcomment>
        <translation>About</translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="49"/>
        <location filename="Editor/Editor.cpp" line="49"/>
        <source>editor.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="52"/>
        <location filename="Editor/Editor.cpp" line="52"/>
        <source>save.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="53"/>
        <location filename="Editor/Editor.cpp" line="53"/>
        <source>quit.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="54"/>
        <location filename="Editor/Editor.cpp" line="54"/>
        <source>cut.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="55"/>
        <location filename="Editor/Editor.cpp" line="55"/>
        <source>copy.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="56"/>
        <location filename="Editor/Editor.cpp" line="56"/>
        <source>paste.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="57"/>
        <location filename="Editor/Editor.cpp" line="57"/>
        <source>undo.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="58"/>
        <location filename="Editor/Editor.cpp" line="58"/>
        <source>redo.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="59"/>
        <location filename="Editor/Editor.cpp" line="59"/>
        <source>printer.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="60"/>
        <location filename="Editor/Editor.cpp" line="60"/>
        <source>help.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="61"/>
        <location filename="Editor/Editor.cpp" line="192"/>
        <location filename="Editor/Editor.cpp" line="61"/>
        <location filename="Editor/Editor.cpp" line="192"/>
        <source>about.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="78"/>
        <location filename="Editor/Editor.cpp" line="99"/>
        <location filename="Editor/Editor.cpp" line="179"/>
        <location filename="Editor/Editor.cpp" line="78"/>
        <location filename="Editor/Editor.cpp" line="99"/>
        <location filename="Editor/Editor.cpp" line="179"/>
        <source>Error</source>
        <translatorcomment>Error</translatorcomment>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="117"/>
        <location filename="Editor/Editor.cpp" line="117"/>
        <source>Salir</source>
        <translatorcomment>Exit</translatorcomment>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="118"/>
        <location filename="Editor/Editor.cpp" line="118"/>
        <source>¿Guardar nota?
</source>
        <translatorcomment>Save note?</translatorcomment>
        <translation>Save note?</translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="174"/>
        <location filename="Editor/Editor.cpp" line="174"/>
        <source>es.txt</source>
        <translatorcomment>en.txt</translatorcomment>
        <translation>en.txt</translation>
    </message>
    <message>
        <location filename="Editor/Editor.cpp" line="202"/>
        <location filename="Editor/Editor.cpp" line="202"/>
        <source>https://www.arper.me/myqnote/help/es.html</source>
        <translatorcomment>https://www.arper.me/myqnote/help/en.html</translatorcomment>
        <translation>https://www.arper.me/myqnote/help/en.html</translation>
    </message>
</context>
<context>
    <name>ListModel</name>
    <message>
        <location filename="Manager/ListModel/ListModel.cpp" line="22"/>
        <location filename="Manager/ListModel/ListModel.cpp" line="22"/>
        <source>No hay lista cargada.</source>
        <translatorcomment>There is no list loaded.</translatorcomment>
        <translation>There is no list loaded.</translation>
    </message>
</context>
<context>
    <name>Manager</name>
    <message>
        <location filename="Manager/manager.ui" line="14"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="187"/>
        <location filename="Manager/manager.ui" line="14"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="187"/>
        <source>myQNote</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="27"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="205"/>
        <location filename="Manager/manager.ui" line="27"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="205"/>
        <source>Mostrar notas:</source>
        <translatorcomment>Show notes:</translatorcomment>
        <translation>Show notes:</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="52"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="206"/>
        <location filename="Manager/manager.ui" line="52"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="206"/>
        <source>Visibles</source>
        <translatorcomment>Visible</translatorcomment>
        <translation>Visible</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="68"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="207"/>
        <location filename="Manager/manager.ui" line="68"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="207"/>
        <source>Ocu&amp;ltas</source>
        <translatorcomment>Hidden</translatorcomment>
        <translation>Hidden</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="75"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="208"/>
        <location filename="Manager/manager.ui" line="75"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="208"/>
        <source>Todas</source>
        <translatorcomment>All</translatorcomment>
        <translation>All</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="101"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="209"/>
        <location filename="Manager/manager.ui" line="101"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="209"/>
        <source>Añadir</source>
        <translatorcomment>Add</translatorcomment>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="110"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="210"/>
        <location filename="Manager/manager.ui" line="110"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="210"/>
        <source>Cambiar nombre</source>
        <translatorcomment>Rename</translatorcomment>
        <translation>Rename</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="117"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="211"/>
        <location filename="Manager/Manager.cpp" line="298"/>
        <location filename="Manager/manager.ui" line="117"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="211"/>
        <location filename="Manager/Manager.cpp" line="298"/>
        <source>Eliminar</source>
        <translatorcomment>Delete</translatorcomment>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="124"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="212"/>
        <location filename="Manager/Manager.cpp" line="179"/>
        <location filename="Manager/Manager.cpp" line="189"/>
        <location filename="Manager/manager.ui" line="124"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="212"/>
        <location filename="Manager/Manager.cpp" line="179"/>
        <location filename="Manager/Manager.cpp" line="189"/>
        <source>Ocultar</source>
        <translatorcomment>Hide</translatorcomment>
        <translation>Hide</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="143"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="213"/>
        <location filename="Manager/manager.ui" line="143"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="213"/>
        <source>A&amp;rchivo</source>
        <translatorcomment>File</translatorcomment>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="152"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="214"/>
        <location filename="Manager/manager.ui" line="152"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="214"/>
        <source>E&amp;ditar</source>
        <translatorcomment>Edit</translatorcomment>
        <translation>&amp;Edit</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="159"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="215"/>
        <location filename="Manager/manager.ui" line="159"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="215"/>
        <source>&amp;Preferencias</source>
        <translatorcomment>Preferences</translatorcomment>
        <translation>&amp;Preferences</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="165"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="216"/>
        <location filename="Manager/manager.ui" line="165"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="216"/>
        <source>A&amp;yuda</source>
        <translatorcomment>Help</translatorcomment>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="177"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="188"/>
        <location filename="Manager/manager.ui" line="177"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="188"/>
        <source>&amp;Añadir nota</source>
        <translatorcomment>Add note</translatorcomment>
        <translation>&amp;Add note</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="182"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="189"/>
        <location filename="Manager/manager.ui" line="182"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="189"/>
        <source>Cambiar &amp;nombre</source>
        <translatorcomment>Rename</translatorcomment>
        <translation>&amp;Rename</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="185"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="191"/>
        <location filename="Manager/manager.ui" line="185"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="191"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="190"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="193"/>
        <location filename="Manager/manager.ui" line="190"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="193"/>
        <source>&amp;Eliminar nota</source>
        <translatorcomment>Delete note</translatorcomment>
        <translation>&amp;Delete note</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="193"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="195"/>
        <location filename="Manager/manager.ui" line="193"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="195"/>
        <source>Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="198"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="197"/>
        <location filename="Manager/manager.ui" line="198"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="197"/>
        <source>&amp;Salir</source>
        <translatorcomment>Exit</translatorcomment>
        <translation>E&amp;xit</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="201"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="199"/>
        <location filename="Manager/manager.ui" line="201"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="199"/>
        <source>Ctrl+Q</source>
        <translatorcomment>Ctrl+W</translatorcomment>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="206"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="201"/>
        <location filename="Manager/manager.ui" line="206"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="201"/>
        <source>&amp;Cambiar Idioma</source>
        <translatorcomment>Change language</translatorcomment>
        <translation>&amp;Change language</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="211"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="202"/>
        <location filename="Manager/manager.ui" line="211"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="202"/>
        <source>&amp;Mostrar/Ocultar</source>
        <translatorcomment>Show / hide</translatorcomment>
        <translation>Show / &amp;hide</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="216"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="203"/>
        <location filename="Manager/manager.ui" line="216"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="203"/>
        <source>Ayuda</source>
        <translatorcomment>Help</translatorcomment>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="Manager/manager.ui" line="221"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="204"/>
        <location filename="Manager/Manager.cpp" line="419"/>
        <location filename="Manager/manager.ui" line="221"/>
        <location filename="compilacion/myQNote_autogen/include/ui_manager.h" line="204"/>
        <location filename="Manager/Manager.cpp" line="419"/>
        <source>Acerca de</source>
        <translatorcomment>About</translatorcomment>
        <translation>About</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="69"/>
        <location filename="Manager/Manager.cpp" line="69"/>
        <source>keeps.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="72"/>
        <location filename="Manager/Manager.cpp" line="72"/>
        <source>help.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="73"/>
        <location filename="Manager/Manager.cpp" line="418"/>
        <location filename="Manager/Manager.cpp" line="73"/>
        <location filename="Manager/Manager.cpp" line="418"/>
        <source>about.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="74"/>
        <location filename="Manager/Manager.cpp" line="81"/>
        <location filename="Manager/Manager.cpp" line="74"/>
        <location filename="Manager/Manager.cpp" line="81"/>
        <source>add.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="75"/>
        <location filename="Manager/Manager.cpp" line="82"/>
        <location filename="Manager/Manager.cpp" line="75"/>
        <location filename="Manager/Manager.cpp" line="82"/>
        <source>del.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="76"/>
        <location filename="Manager/Manager.cpp" line="76"/>
        <source>quit.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="77"/>
        <location filename="Manager/Manager.cpp" line="83"/>
        <location filename="Manager/Manager.cpp" line="77"/>
        <location filename="Manager/Manager.cpp" line="83"/>
        <source>rename.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="78"/>
        <location filename="Manager/Manager.cpp" line="84"/>
        <location filename="Manager/Manager.cpp" line="78"/>
        <location filename="Manager/Manager.cpp" line="84"/>
        <source>visibility.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="79"/>
        <location filename="Manager/Manager.cpp" line="79"/>
        <source>language.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="104"/>
        <location filename="Manager/Manager.cpp" line="104"/>
        <source>Directorio</source>
        <translatorcomment>Directory</translatorcomment>
        <translation>Directory</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="104"/>
        <location filename="Manager/Manager.cpp" line="104"/>
        <source>El directorio no existe</source>
        <translatorcomment>The directory does not exist</translatorcomment>
        <translation>The directory does not exist</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="120"/>
        <location filename="Manager/Manager.cpp" line="120"/>
        <source>Idiomas</source>
        <translatorcomment>Language</translatorcomment>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="120"/>
        <location filename="Manager/Manager.cpp" line="120"/>
        <source>No se han encontrado idiomas.
Idioma por defecto.</source>
        <translatorcomment>No languages found.\nDefault language.</translatorcomment>
        <translation>No languages found.\nDefault language.</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="130"/>
        <location filename="Manager/Manager.cpp" line="130"/>
        <source>Lenguaje</source>
        <translatorcomment>Language</translatorcomment>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="130"/>
        <location filename="Manager/Manager.cpp" line="130"/>
        <source>Selecciona el idioma: </source>
        <translatorcomment>Select the language: </translatorcomment>
        <translation>Select the language: </translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="134"/>
        <location filename="Manager/Manager.cpp" line="134"/>
        <source>Idioma</source>
        <translatorcomment>Language</translatorcomment>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="134"/>
        <location filename="Manager/Manager.cpp" line="134"/>
        <source>No se ha podido cambiar el idioma.
Idioma por defecto.</source>
        <translatorcomment>Could not change language.\nDefault language.</translatorcomment>
        <translation>Could not change language.\nDefault language.</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="178"/>
        <location filename="Manager/Manager.cpp" line="201"/>
        <location filename="Manager/Manager.cpp" line="178"/>
        <location filename="Manager/Manager.cpp" line="201"/>
        <source>Mostrar</source>
        <translatorcomment>Show</translatorcomment>
        <translation>Show</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="261"/>
        <location filename="Manager/Manager.cpp" line="261"/>
        <source>Nombre de la nota</source>
        <translatorcomment>Note name</translatorcomment>
        <translation>Note name</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="262"/>
        <location filename="Manager/Manager.cpp" line="262"/>
        <source>Nombre:</source>
        <translatorcomment>Name:</translatorcomment>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="267"/>
        <location filename="Manager/Manager.cpp" line="335"/>
        <location filename="Manager/Manager.cpp" line="267"/>
        <location filename="Manager/Manager.cpp" line="335"/>
        <source>Nombre no válido.</source>
        <translatorcomment>Invalid Name.</translatorcomment>
        <translation>Invalid Name.</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="268"/>
        <location filename="Manager/Manager.cpp" line="336"/>
        <location filename="Manager/Manager.cpp" line="268"/>
        <location filename="Manager/Manager.cpp" line="336"/>
        <source>El nombre no puede estar vacio ni empezar con &apos;.&apos;</source>
        <translatorcomment>The name cannot be empty or start with &apos;.&apos;</translatorcomment>
        <translation>The name cannot be empty or start with &apos;.&apos;</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="294"/>
        <location filename="Manager/Manager.cpp" line="305"/>
        <location filename="Manager/Manager.cpp" line="320"/>
        <location filename="Manager/Manager.cpp" line="354"/>
        <location filename="Manager/Manager.cpp" line="294"/>
        <location filename="Manager/Manager.cpp" line="305"/>
        <location filename="Manager/Manager.cpp" line="320"/>
        <location filename="Manager/Manager.cpp" line="354"/>
        <source>Info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="294"/>
        <location filename="Manager/Manager.cpp" line="320"/>
        <location filename="Manager/Manager.cpp" line="354"/>
        <location filename="Manager/Manager.cpp" line="294"/>
        <location filename="Manager/Manager.cpp" line="320"/>
        <location filename="Manager/Manager.cpp" line="354"/>
        <source>El archivo no existe</source>
        <translatorcomment>The file does not exists</translatorcomment>
        <translation>The file does not exists</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="299"/>
        <location filename="Manager/Manager.cpp" line="299"/>
        <source>¿Eliminar nota?
</source>
        <translatorcomment>Delete note?</translatorcomment>
        <translation>Delete note?</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="305"/>
        <location filename="Manager/Manager.cpp" line="305"/>
        <source>La nota no se ha podido eliminar.</source>
        <translatorcomment>The note could not be deleted.</translatorcomment>
        <translation>The note could not be deleted.</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="329"/>
        <location filename="Manager/Manager.cpp" line="329"/>
        <source>Renombrar</source>
        <translatorcomment>Rename</translatorcomment>
        <translation>Rename</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="330"/>
        <location filename="Manager/Manager.cpp" line="330"/>
        <source>Nuevo nombre:</source>
        <translatorcomment>New name:</translatorcomment>
        <translation>New name:</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="400"/>
        <location filename="Manager/Manager.cpp" line="400"/>
        <source>es.txt</source>
        <translation>en.txt</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="405"/>
        <location filename="Manager/Manager.cpp" line="405"/>
        <source>Error</source>
        <translatorcomment>Error</translatorcomment>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="Manager/Manager.cpp" line="428"/>
        <location filename="Manager/Manager.cpp" line="428"/>
        <source>https://www.arper.me/myqnote/help/es.html</source>
        <translatorcomment>https://www.arper.me/myqnote/help/en.html</translatorcomment>
        <translation>https://www.arper.me/myqnote/help/en.html</translation>
    </message>
</context>
</TS>
